# GlDraggable Component

Draft for drag and drop component for dashboards... and more! 

Required for: 

[https://gitlab.com/gitlab-org/gitlab-ce/issues/60011](https://gitlab.com/gitlab-org/gitlab-ce/issues/60011)

## Goal

Decide if this should be a new component part of GitLab UI.

## Usage

```html
<gl-draggable v-model="list" :disabled="disabled" group="myDashboard">
  <div class="element" v-for="element in list" :key="element.id">
    {{ element.name }}
  </div>
</gl-draggable>
```

## Project setup

```sh
cd vue/
yarn install
```

### Compiles and hot-reloads for development

```sh
cd vue/
yarn serve
```

### Compiles and minifies for production

```sh
cd vue/
yarn build # gets built in ../public
```
